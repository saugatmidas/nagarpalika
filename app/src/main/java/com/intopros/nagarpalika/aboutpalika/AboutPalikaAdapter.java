package com.intopros.nagarpalika.aboutpalika;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.incidentregistry.birth.BirthActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AboutPalikaAdapter extends AbstractAdapter {
    public AboutPalikaAdapter(List<StaffModel> stafflist, Activity activity) {
        this.activity = activity;
        this.mItemList = stafflist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view =inflater.inflate(R.layout.row_about_staffs,viewGroup,false);
        return new StaffView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        StaffView holder = (StaffView) viewHolder;
        StaffModel object = (StaffModel) mItemList.get(position);
        holder.setUpViews(object.getName(),object.getPost(),object.getImage());

    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
