package com.intopros.nagarpalika.aboutpalika;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.GridSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class AboutStaffActivity extends AbstractActivity {
    @BindView(R.id.aboutPalikaRecycler)
    RecyclerView recycler;
    AboutPalikaAdapter adapter;
    @Override
    public int setLayout() {
        return R.layout.activity_about_palika;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("About Palika", true);
        List<StaffModel> staffList = new ArrayList<>();
        staffList.add(new StaffModel("1","https://d3i6fh83elv35t.cloudfront.net/static/2019/03/RTX6P9YW-1200x800.jpg","Elon Musk","Vice Chairman"));
        staffList.add(new StaffModel("1","https://timedotcom.files.wordpress.com/2018/09/bill-gates-africa.jpg","Bill Gates","Member"));
        staffList.add(new StaffModel("1","https://assets.entrepreneur.com/content/3x2/2000/20190607142532-jeff-bezos.jpeg?width=700&crop=2:1","Jeff Bezos","Member"));
        staffList.add(new StaffModel("1","https://i1.wp.com/fifthperson.com/wp-content/uploads/2015/04/warren-buffett.jpg?resize=620%2C388&ssl=1","Warren Buffett","Member"));
        staffList.add(new StaffModel("1","https://specials-images.forbesimg.com/imageserve/5c76b7d331358e35dd2773a9/416x416.jpg?background=000000&cropX1=0&cropX2=4401&cropY1=0&cropY2=4401","Mark Zuckerberg","Member"));
        staffList.add(new StaffModel("1","https://image.cnbcfm.com/api/v1/image/100496736-steve-jobs-march-2011-getty.jpg?v=1513863842&w=1400&h=950","Steve Jobs","Member"));
        staffList.add(new StaffModel("1","https://amp.businessinsider.com/images/5b748ea80ce5f519008b55fa-960-720.jpg","Larry Page","Member"));
        staffList.add(new StaffModel("1","https://image.cnbcfm.com/api/v1/image/104225995-_95A5004.jpg?v=1540458420&w=1400&h=950","Jack Ma","Member"));
        staffList.add(new StaffModel("1","https://tedideas.files.wordpress.com/2016/04/featured_torvalds_linux_ted_31.jpg?w=750","Linus Trovalds","Member"));
        staffList.add(new StaffModel("1","https://ethw.org/w/images/9/97/Dennis_M._Ritchie_2260.jpg","Dennis Ritchie","Member"));
        recycler.setLayoutManager(new GridLayoutManager(getActivityContext(),3));
        adapter = new AboutPalikaAdapter(staffList,getActivityContext());
        recycler.setAdapter(adapter);
        recycler.addItemDecoration(new GridSpaceItemDecoration(20));
    }
}
