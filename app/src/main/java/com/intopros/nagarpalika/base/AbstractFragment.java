package com.intopros.nagarpalika.base;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.base.baseutils.sharedpreferences.SharedPreferencesHelper;

import butterknife.ButterKnife;

public abstract class AbstractFragment extends Fragment {

    public View rootView;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(setLayout(), container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, rootView);
        if (!fragmentResume && fragmentVisible) {   //only when first time fragment is created
            fragmentIsVisible();
        }
        activityCreated();
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            fragmentIsVisible();
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
            fragmentGone();
        }
    }

    public void fragmentGone() {
    }

    public void fragmentIsVisible() {
    }

    public abstract Activity getActivityContext();

    public abstract int setLayout();

    public abstract void activityCreated();

    public String getPref(String key) {
        if (getActivityContext() != null) {
            return SharedPreferencesHelper.getSharedPreferences(getActivityContext(), key, "");
        } else {
            return "";
        }
    }

    public Boolean getBoolPref(String key) {
        if (getActivityContext() != null) {
            return SharedPreferencesHelper.getSharedPreferences(getActivityContext(), key, false);
        } else {
            return false;
        }
    }

    public void setPref(String key, String value) {
        if (getActivityContext() != null) {
            SharedPreferencesHelper.setSharedPreferences(getActivityContext(), key, value);
        }
    }

    public void setPref(String key, Boolean value) {
        if (getActivityContext() != null) {
            SharedPreferencesHelper.setSharedPreferences(getActivityContext(), key, value);
        }
    }

}
