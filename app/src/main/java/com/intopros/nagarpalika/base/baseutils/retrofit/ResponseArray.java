package com.intopros.nagarpalika.base.baseutils.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseArray<T> {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private List<T> response = null;
    @SerializedName("extraparams")
    @Expose
    private ExtraParamObject extraparams;
    @SerializedName("link")
    @Expose
    private Boolean link;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getResponse() {
        return response;
    }

    public void setResponse(List<T> response) {
        this.response = response;
    }

    public ExtraParamObject getExtraparams() {
        return extraparams;
    }

    public void setExtraparams(ExtraParamObject extraparams) {
        this.extraparams = extraparams;
    }

    public Boolean getLink() {
        return link;
    }

    public void setLink(Boolean link) {
        this.link = link;
    }
}
