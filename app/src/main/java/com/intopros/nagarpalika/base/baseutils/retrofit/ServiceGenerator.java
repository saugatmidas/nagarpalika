package com.intopros.nagarpalika.base.baseutils.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intopros.nagarpalika.BuildConfig;
import com.intopros.nagarpalika.base.RequestHeader;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addNetworkInterceptor(RequestHeader.getHeader())
            .addInterceptor(Interceptor())
            .build();

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URLs.host)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static ApiService service = retrofit.create(ApiService.class);

    private static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    public static ApiService createService() {
        return service;
    }

    public static Gson getGson() {
        return gson;
    }

    public static HttpLoggingInterceptor Interceptor() {
        // defining interceptor to print request to log
        HttpLoggingInterceptor interceptor;
        if (BuildConfig.DEBUG)
            interceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);
        else
            interceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.NONE);
        ;
        return interceptor;
    }
}
