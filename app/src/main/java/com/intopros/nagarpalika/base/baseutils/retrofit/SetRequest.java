package com.intopros.nagarpalika.base.baseutils.retrofit;

import android.app.Activity;
import android.app.AlertDialog;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.baseutils.Checker;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class SetRequest {

    Call<JsonObject> call;
    Gson g = null;
    Activity a;
    Boolean pd = false;
    AlertDialog pDialog;

    SetRequest SetRequest() {
        return this;
    }

    public SetRequest set(Call<JsonObject> c) {
        call = c;
        return this;
    }

    public SetRequest get(Activity a) {
        this.a = a;
        pDialog = new SpotsDialog.Builder().setContext(a).setTheme(R.style.CustomDialogTheme).build();
        g = ServiceGenerator.getGson();
        return this;
    }

    public void cancel() {
        if (call != null) {
            call.cancel();
        }
    }

    public SetRequest pdialog(String msg, Boolean b) {
        pd = true;
        pDialog.setMessage(msg);
        pDialog.show();
        pDialog.setCancelable(b);
        pDialog.setCanceledOnTouchOutside(b);
        return this;
    }

    public SetRequest pdialog(Boolean b) {
        pd = true;
        pDialog.setMessage("Loading ...");
        pDialog.show();
        pDialog.setCancelable(b);
        pDialog.setCanceledOnTouchOutside(b);
        return this;
    }

    public SetRequest pdialog() {
        pd = true;
        pDialog.setMessage("Loading ...");
        pDialog.show();
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        return this;
    }

    public SetRequest start(final OnRes r) {
        if (Checker.NetworkAvailable(a)) {
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                    if (pd) {
                        pDialog.dismiss();
                    }
                    String msg = a.getResources().getString(R.string.please_try_again);

                    switch (response.code()) {
                        case 200:
                            //success
                            r.OnSuccess(response.body());
                            break;
                        case 204:
                            //data not available
                            r.OnError(a.getString(R.string.no_data), "ND", response.code(), response.body());
                            break;
                        case 400:
                            //bad request
                            r.OnError(msg, "S", response.code(), response.body());
                            break;
                        case 401:
                            //unauthorized
                            r.OnError(msg, "S", response.code(), response.body());

                            break;
                        case 402:
                            //payment required
                            r.OnError(msg, "S", response.code(), response.body());

                            break;
                        default:
                            r.OnError(a.getString(R.string.please_try_again), "D", response.code(), response.body());
                            break;
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    if (pd) {
                        pDialog.dismiss();
                    }
                    r.OnError(a.getString(R.string.please_try_again), "D", 0, null);
                }
            });
        } else {
            if (pd) {
                pDialog.dismiss();
            }
            r.OnError(a.getString(R.string.connect_to_internet), "N", 1, null);
        }
        return this;
    }

}
