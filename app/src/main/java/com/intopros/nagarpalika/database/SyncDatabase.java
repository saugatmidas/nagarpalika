package com.intopros.nagarpalika.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.intopros.nagarpalika.base.AppController;

public class SyncDatabase {

    public String getApiToken() {
        try {
            return AppController.getQuery(UserInfoTable.class).where(UserInfoTableDao.Properties.ApiToken.isNotNull()).limit(1).build().unique().getApiToken();
        } catch (Exception e) {
            return "";
        }
    }

    public static void clearDatabase(Context context) {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(
                context.getApplicationContext(), "stampfee-db", null);
        SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        devOpenHelper.onUpgrade(db, 0, 0);
    }

}


