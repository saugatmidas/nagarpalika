package com.intopros.nagarpalika.dishhome;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.StaticValues;

import butterknife.BindView;

public class DishHomeActivity extends AbstractActivity {
    @BindView(R.id.dishHomeAmountSpinner)
    Spinner dishHomeAmountSpinner;
    @Override
    public int setLayout() {
        return R.layout.activity_dishhome;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Dish Home",true);
        setUpSpinner();
    }

    private void setUpSpinner() {
        ArrayAdapter<String> dishHomeAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getAmounts());
        dishHomeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dishHomeAmountSpinner.setAdapter(dishHomeAdapter);

    }
}
