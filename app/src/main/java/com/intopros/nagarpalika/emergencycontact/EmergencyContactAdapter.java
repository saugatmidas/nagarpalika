package com.intopros.nagarpalika.emergencycontact;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;

import java.util.List;

public class EmergencyContactAdapter extends AbstractAdapter<EmergencyContactModel> {


    public EmergencyContactAdapter(List<EmergencyContactModel> list) {
        mItemList = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new EmergencyContactView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_emergency_contact, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        EmergencyContactView holder = (EmergencyContactView) viewHolder;
        holder.setItem(mItemList.get(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
