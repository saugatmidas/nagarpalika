package com.intopros.nagarpalika.findwoda;

public class WodaModel {

    String wodaName;
    double lng;
    double lat;

    public WodaModel(String wodaName, double lat, double lng) {
        this.wodaName = wodaName;
        this.lat = lat;
        this.lng = lng;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getWodaName() {
        return wodaName;
    }

    public void setWodaName(String wodaName) {
        this.wodaName = wodaName;
    }

}
