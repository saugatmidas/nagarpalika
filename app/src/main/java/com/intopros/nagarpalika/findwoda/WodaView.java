package com.intopros.nagarpalika.findwoda;

import android.view.View;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WodaView extends RecyclerView.ViewHolder {

    public View rootView;
    @BindView(R.id.tv_ward)
    TextView tv_ward;

    public WodaView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setItem(WodaModel item) {
        tv_ward.setText(item.getWodaName());
    }

    public void makeSelection(boolean selected) {
        if (selected) {
            tv_ward.setBackgroundColor(rootView.getContext().getResources().getColor(R.color.colorPrimaryDark));
            tv_ward.setTextColor(rootView.getContext().getResources().getColor(R.color.colorWhite));
        } else {
            tv_ward.setBackgroundColor(rootView.getContext().getResources().getColor(R.color.colorWhite));
            tv_ward.setTextColor(rootView.getContext().getResources().getColor(R.color.colorBlack));
        }
    }

}
