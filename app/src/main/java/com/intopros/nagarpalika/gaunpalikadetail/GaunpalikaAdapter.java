package com.intopros.nagarpalika.gaunpalikadetail;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;

import java.util.List;

public class GaunpalikaAdapter extends AbstractAdapter<GaunpalikaObject> {

    public GaunpalikaAdapter(List<GaunpalikaObject> List) {
        mItemList = List;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_gaunpalika_detail, viewGroup, false);
        return new GaunpalikaView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        GaunpalikaView holder = (GaunpalikaView) viewHolder;
        holder.setItem(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

}
