package com.intopros.nagarpalika.gaunpalikadetail;

public class GaunpalikaObject {

    String title;
    String description;

    public GaunpalikaObject(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
