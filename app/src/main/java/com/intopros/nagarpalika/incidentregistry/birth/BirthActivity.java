package com.intopros.nagarpalika.incidentregistry.birth;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.StaticValues;

import butterknife.BindView;

public class BirthActivity extends AbstractActivity {
    @BindView(R.id.yearSpinner)
    Spinner yearSpinner;
    @BindView(R.id.monthSpinner)
    Spinner monthSpinner;
    @BindView(R.id.daySpinner)
    Spinner daySpinner;
    @BindView(R.id.adYearSpinner)
    Spinner adYearSpinner;
    @BindView(R.id.adMonthSpinner)
    Spinner adMonthSpinner;
    @BindView(R.id.adDaySpinner)
    Spinner adDaySpinner;
    @Override
    public int setLayout() {
        return R.layout.activity_incident_registry_birth;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Birth Report",true);
        hideShadow();
        setSpinner();
    }
    private void setSpinner() {
        ArrayAdapter<String> yearAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDate());
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearAdapter);

        ArrayAdapter<String> monthAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getMonth());
        monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(monthAdapter);

        ArrayAdapter<String> dayAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDay());
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(dayAdapter);

        ArrayAdapter<String> adYearAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDate());
        adYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adYearSpinner.setAdapter(adYearAdapter);

        ArrayAdapter<String> adMonthAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getMonth());
        adMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adMonthSpinner.setAdapter(adMonthAdapter);

        ArrayAdapter<String> adDayAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDay());
        adDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adDaySpinner.setAdapter(adDayAdapter);


    }
}
