package com.intopros.nagarpalika.incidentregistry.death;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.StaticValues;

import butterknife.BindView;

public class DeathActivity extends AbstractActivity {
    @BindView(R.id.yearBornSpinner)
    Spinner yearBornSpinner;
    @BindView(R.id.adBornYearSpinner)
    Spinner adBornYearSpinner;
    @BindView(R.id.monthBornSpinner)
    Spinner monthBornSpinner;
    @BindView(R.id.adBornMonthSpinner)
    Spinner adBornMonthSpinner;
    @BindView(R.id.dayBornSpinner)
    Spinner dayBornSpinner;
 @BindView(R.id.adBornDaySpinner)
    Spinner adBornDaySpinner;

    @BindView(R.id.yearDeathSpinner)
    Spinner yearDeathSpinner;
    @BindView(R.id.adDeathYearSpinner)
    Spinner adDeathYearSpinner;
    @BindView(R.id.monthDeathSpinner)
    Spinner monthDeathSpinner;
    @BindView(R.id.adDeathMonthSpinner)
    Spinner adDeathMonthSpinner;
    @BindView(R.id.dayDeathSpinner)
    Spinner dayDeathSpinner;
    @BindView(R.id.adDeathDaySpinner)
    Spinner adDeathDaySpinner;

    @Override
    public int setLayout() {
        return R.layout.activity_incident_registry_death;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Death Report",true);
        hideShadow();
        setUpSpinners();
    }

    private void setUpSpinners() {
        ArrayAdapter<String> birthYearAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDate());
        birthYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearBornSpinner.setAdapter(birthYearAdapter);

        ArrayAdapter<String> birthMonthAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getMonth());
        birthMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthBornSpinner.setAdapter(birthMonthAdapter);

        ArrayAdapter<String> birthDayAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDay());
        birthDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dayBornSpinner.setAdapter(birthDayAdapter);

        ArrayAdapter<String> adBornYearAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDate());
        adBornYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adBornYearSpinner.setAdapter(adBornYearAdapter);

        ArrayAdapter<String> adBornMonthAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getMonth());
        adBornMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adBornMonthSpinner.setAdapter(adBornMonthAdapter);

        ArrayAdapter<String> adBornDayAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDay());
        adBornDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adBornDaySpinner.setAdapter(adBornDayAdapter);



        ArrayAdapter<String> deathYearAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDate());
        deathYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearDeathSpinner.setAdapter(deathYearAdapter);

        ArrayAdapter<String> deathMonthAdapter=new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getMonth());
        deathMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthDeathSpinner.setAdapter(deathMonthAdapter);

        ArrayAdapter<String> deathDayAdapter=new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDay());
        deathDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dayDeathSpinner.setAdapter(deathDayAdapter);

        ArrayAdapter<String> adDeathYearAdapter=new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDate());
        adDeathYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adDeathYearSpinner.setAdapter(adDeathYearAdapter);

        ArrayAdapter<String> adDeathMonthAdapter=new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getMonth());
        adDeathMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adDeathMonthSpinner.setAdapter(adDeathMonthAdapter);

        ArrayAdapter<String> adDeathDayAdapter=new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getDay());
        adDeathDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adDeathDaySpinner.setAdapter(adDeathDayAdapter);
    }
}
