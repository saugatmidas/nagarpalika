package com.intopros.nagarpalika.incidentregistry.migration;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class MigrationActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_incident_registry_migration;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Migration Report",true);
        hideShadow();
    }
}
