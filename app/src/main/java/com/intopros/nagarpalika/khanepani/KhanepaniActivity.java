package com.intopros.nagarpalika.khanepani;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.StaticValues;

import butterknife.BindView;

public class KhanepaniActivity extends AbstractActivity{
    @BindView(R.id.khanepaniCounterSpinner)
    Spinner khanepaniCounterSpinner;
    @Override
    public int setLayout() {
        return R.layout.activity_khanepani;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Khanepani",true);
        setUpSpinner();

    }

        private void setUpSpinner() {
            ArrayAdapter<String> counterAdapter =new  ArrayAdapter<>(getActivityContext(),R.layout.support_simple_spinner_dropdown_item, StaticValues.getCounters());
            counterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            khanepaniCounterSpinner.setAdapter(counterAdapter);

        }
}
