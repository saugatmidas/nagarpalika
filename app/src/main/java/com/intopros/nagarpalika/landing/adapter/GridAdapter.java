package com.intopros.nagarpalika.landing.adapter;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.adsl.AdslActivity;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.dishhome.DishHomeActivity;
import com.intopros.nagarpalika.incidentregistry.IncidentRegistryActivity;
import com.intopros.nagarpalika.incidentregistry.IncidentRegistryAdapter;
import com.intopros.nagarpalika.incidentregistry.birth.BirthActivity;
import com.intopros.nagarpalika.khanepani.KhanepaniActivity;
import com.intopros.nagarpalika.landing.model.MenuModel;
import com.intopros.nagarpalika.landing.viewholder.TileView;
import com.intopros.nagarpalika.naksa.NaksaPassActivity;
import com.intopros.nagarpalika.nea.NeaActivity;
import com.intopros.nagarpalika.sifarisdarta.SifarisMenuActivity;
import com.intopros.nagarpalika.smartwallet.SmartWalletActivity;
import com.intopros.nagarpalika.topup.TopUpActivity;
import com.intopros.nagarpalika.vianet.VianetActivity;
import com.intopros.nagarpalika.worldlink.WorldLinkActivity;

import java.util.List;

public class GridAdapter extends AbstractAdapter<MenuModel> {

    public GridAdapter(Activity activity, List<MenuModel> menuList) {
        this.activity = activity;
        mItemList = menuList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        return new TileView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tiles, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        TileView holder = (TileView) viewHolder;
        holder.setTitle(mItemList.get(position).getTitle());
        holder.setIcon(mItemList.get(position).getImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemList.get(position).getImage() == R.drawable.ic_write) {
                    activity.startActivity(new Intent(activity, IncidentRegistryActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_recommendation) {
                    activity.startActivity(new Intent(activity, SifarisMenuActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_electricity) {
                    activity.startActivity(new Intent(activity, NeaActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_tap) {
                    activity.startActivity(new Intent(activity, KhanepaniActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_smartphone) {
                    activity.startActivity(new Intent(activity, TopUpActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_satellite_tv) {
                    activity.startActivity(new Intent(activity, DishHomeActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_electric_tower) {
                    activity.startActivity(new Intent(activity, AdslActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_wallet_filled_money_tool) {
                    activity.startActivity(new Intent(activity, SmartWalletActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_sketch) {
                    activity.startActivity(new Intent(activity, NaksaPassActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.vianet) {
                    activity.startActivity(new Intent(activity, VianetActivity.class));
                } else if (mItemList.get(position).getImage() == R.drawable.ic_worldlink) {
                    activity.startActivity(new Intent(activity, WorldLinkActivity.class));
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
