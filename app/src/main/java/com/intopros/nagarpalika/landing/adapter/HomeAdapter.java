package com.intopros.nagarpalika.landing.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.landing.model.HomeModel;
import com.intopros.nagarpalika.landing.viewholder.HomeButtonView;
import com.intopros.nagarpalika.landing.viewholder.ImageSliderView;
import com.intopros.nagarpalika.landing.viewholder.MenuView;
import com.intopros.nagarpalika.landing.viewholder.StatsView;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends AbstractAdapter<HomeModel> {

    public final int SLIDER = 0;
    public final int STATS = 1;
    public final int MENU = 2;
    public final int BUTTON = 3;

    public HomeAdapter(Activity a, List<HomeModel> mlist) {
        this.activity = a;
        this.mItemList = mlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        if (position == SLIDER) {
            return new ImageSliderView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_slider, parent, false));
        } else if (position == MENU) {
            return new MenuView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recyclerview, parent, false));
        } else if (position == BUTTON) {
            return new HomeButtonView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_button, parent, false));
        }else if(position == STATS){
            return new StatsView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_nagarpalika_stats,parent,false));
        } else
            return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ImageSliderView) {
            ImageSliderView holder = (ImageSliderView) viewHolder;
            List<String> images = new ArrayList<>(2);
            images.add("https://pbs.twimg.com/media/DfONDXoVMAAIl5F.jpg");
            images.add("https://lh3.googleusercontent.com/arl8pnzV5n5ko_8R-tKpiB6qoHueuy_rMBOulWceAkDIiuGVyoLHfQJiSN82mDcGzOI");
            holder.setItem(images);
        } else if (viewHolder instanceof MenuView) {
            MenuView holder = (MenuView) viewHolder;
            holder.setTitle(mItemList.get(position).getTitle());
            holder.setAdapter(new GridAdapter(activity, mItemList.get(position).getMenuList()));
        } else if (viewHolder instanceof HomeButtonView) {
            HomeButtonView holder = (HomeButtonView) viewHolder;
            holder.setItem(mItemList.get(position));
        } else if (viewHolder instanceof StatsView){
            StatsView holder = (StatsView) viewHolder;
        }
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    @Override
    public int getItemViewType(int position) {


            switch (mItemList.get(position).getType()) {
                case "slider":
                    return SLIDER;
                case "menu":
                    return MENU;
                case "button":
                    return BUTTON;
                case "stats":
                    return STATS;
                default:
                    return SLIDER;
        }
    }
}
