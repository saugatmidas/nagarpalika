package com.intopros.nagarpalika.landing.adapter;

import com.intopros.nagarpalika.R;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class MainSliderAdapter extends SliderAdapter {
    List<String> banner = new ArrayList<>();

    public MainSliderAdapter(List<String> banner) {
        this.banner = banner;
    }

    @Override
    public int getItemCount() {
        return banner.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(banner.get(position), R.drawable.background_texture, R.drawable.background_texture);
    }
}