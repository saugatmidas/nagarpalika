package com.intopros.nagarpalika.landing.viewholder;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.aboutpalika.AboutStaffActivity;
import com.intopros.nagarpalika.landing.model.HomeModel;
import com.intopros.nagarpalika.placesinpalika.PlacesActivity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeButtonView extends RecyclerView.ViewHolder {

    View rootView;
    @BindView(R.id.btn)
    public Button btn;

    public HomeButtonView(@NonNull View itemView) {
        super(itemView);
        rootView = itemView;
        ButterKnife.bind(this, rootView);
    }

    public void setItem(HomeModel item) {
        btn.setText(rootView.getResources().getString(item.getTitleId()));
        btn.setOnClickListener(v -> {
            switch (item.getTitleId()) {
                case R.string.static_places:
                    rootView.getContext().startActivity(new Intent(rootView.getContext(), PlacesActivity.class));
                    break;
                case R.string.static_about_palika:
                    rootView.getContext().startActivity(new Intent(rootView.getContext(), AboutStaffActivity.class));
                    break;
                default:
                    break;
            }
        });
    }


}
