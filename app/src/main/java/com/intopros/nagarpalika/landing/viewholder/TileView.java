package com.intopros.nagarpalika.landing.viewholder;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TileView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.menutitle)
    TextView menutitle;
    @BindView(R.id.menuicon)
    ImageView menuicon;

    public TileView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setTitle(String txt) {
        menutitle.setText(txt);
    }

    public void setBackground(int color) {
//        ViewCompat.setBackgroundTintList(cntn,ColorStateList.valueOf(rView.getResources().getColor(color)));
        ColorStateList csl = AppCompatResources.getColorStateList(rView.getContext(), color);
        Drawable drawable = DrawableCompat.wrap(menuicon.getBackground());
        DrawableCompat.setTintList(drawable, csl);
        menuicon.setBackground(drawable);
        menutitle.setTextColor(ContextCompat.getColor(rView.getContext(), color));
    }

    public void setIcon(int icon) {
        menuicon.setImageDrawable(ResourcesCompat.getDrawable(rView.getResources(), icon, null));
    }
}
