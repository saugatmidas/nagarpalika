package com.intopros.nagarpalika.naksa;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class NaksaPassActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_naksapass;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.static_map),true);
    }
}
