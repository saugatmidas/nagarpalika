package com.intopros.nagarpalika.notification;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.nagarpalika.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageNotificationView extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_icon)
    ImageView iv_icon;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.iv_image_content)
    ImageView iv_image_content;

    View rootView;

    public ImageNotificationView(@NonNull View itemView) {
        super(itemView);
        rootView = itemView;
        ButterKnife.bind(this, rootView);
        //todo put this code inside set item

    }

    public void setItem(final NotificationObject item) {
        Glide.with(rootView).load(item.getLogo())
                .apply(new RequestOptions().centerInside().placeholder(R.drawable.ic_notifications))
                .into(iv_icon);
        tv_title.setText(item.getTitle());
        tv_description.setText(item.getDescription());
        tv_date.setText(item.getPosteddate());
        Glide.with(rootView)
                .load(item.getImage())
                .apply(new RequestOptions().centerCrop())
                .into(iv_image_content);

        iv_image_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  rootView.getContext().startActivity(new Intent(rootView.getContext(), ImageViewActivity.class)
                        .putExtra("image", item.getImage()));*/
            }
        });

    }
}