package com.intopros.nagarpalika.notification;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NoticeFragment extends AbstractFragment {

    @BindView(R.id.rv_notice)
    RecyclerView rv_notice;

    NotificationAdapter mAdapter;
    List<NotificationObject> notificationList = new ArrayList<>();

    @Override
    public Activity getActivityContext() {
        return getActivity();
    }

    @Override
    public int setLayout() {
        return R.layout.fragment_notification;
    }

    @Override
    public void activityCreated() {
        rv_notice.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        loadData();
        mAdapter = new NotificationAdapter(getActivityContext(), notificationList);
        rv_notice.setAdapter(mAdapter);
    }

    private void loadData() {
        notificationList.add(new NotificationObject("image"));
        notificationList.add(new NotificationObject("image"));
        notificationList.add(new NotificationObject("image"));
        notificationList.add(new NotificationObject("image"));
        notificationList.add(new NotificationObject("image"));
        notificationList.add(new NotificationObject("image"));
        notificationList.add(new NotificationObject("image"));
    }
}
