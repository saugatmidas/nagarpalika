package com.intopros.nagarpalika.notification;

import android.app.Activity;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.NonSwipeableViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotificationActivity extends AbstractActivity {

    @BindView(R.id.tabs)
    public TabLayout tabLayout;
    @BindView(R.id.viewpager)
    public NonSwipeableViewPager viewPager;

    @Override
    public int setLayout() {
        return R.layout.activity_notification;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("सुुुुचनाहरु", true);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NoticeFragment(), getString(R.string.static_notification));
        adapter.addFragment(new AnnouncementFragment(), getString(R.string.static_announcement));
        viewPager.setOffscreenPageLimit(adapter.getCount());
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
