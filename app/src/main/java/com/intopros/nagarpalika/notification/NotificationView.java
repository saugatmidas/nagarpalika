package com.intopros.nagarpalika.notification;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.nagarpalika.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationView extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_icon)
    ImageView iv_icon;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_description)
    TextView tv_description;
    @BindView(R.id.tv_date)
    TextView tv_date;

    View rootView;

    public NotificationView(@NonNull View itemView) {
        super(itemView);
        rootView = itemView;
        ButterKnife.bind(this, rootView);
    }

    public void setItem(NotificationObject item) {
        Glide.with(rootView).load(item.getLogo())
                .apply(new RequestOptions().centerInside().placeholder(R.drawable.ic_notifications))
                .into(iv_icon);
        tv_title.setText(item.getTitle());
        tv_description.setText(item.getDescription());
        tv_date.setText(item.getPosteddate());
    }
}
