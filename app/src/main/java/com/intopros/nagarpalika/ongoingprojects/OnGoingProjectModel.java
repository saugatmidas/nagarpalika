package com.intopros.nagarpalika.ongoingprojects;

import java.io.Serializable;

public class OnGoingProjectModel implements Serializable {
    String id;
    String projectName;
    String budget;
    String starting_date;
    String completion_date;
    String timespan;
    String location;
    String supervisor;
    String image;
    String description;

    public OnGoingProjectModel(String id, String projectName, String budget, String starting_date, String completion_date, String timespan, String location, String supervisor, String image, String description) {
        this.id = id;
        this.projectName = projectName;
        this.budget = budget;
        this.starting_date = starting_date;
        this.completion_date = completion_date;
        this.timespan = timespan;
        this.location = location;
        this.supervisor = supervisor;
        this.image = image;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getStarting_date() {
        return starting_date;
    }

    public void setStarting_date(String starting_date) {
        this.starting_date = starting_date;
    }

    public String getCompletion_date() {
        return completion_date;
    }

    public void setCompletion_date(String completion_date) {
        this.completion_date = completion_date;
    }

    public String getTimespan() {
        return timespan;
    }

    public void setTimespan(String timespan) {
        this.timespan = timespan;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
