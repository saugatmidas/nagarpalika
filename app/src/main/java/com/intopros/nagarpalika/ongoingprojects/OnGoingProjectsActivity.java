package com.intopros.nagarpalika.ongoingprojects;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.StaticValues;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class OnGoingProjectsActivity extends AbstractActivity {
    @BindView(R.id.ongoingprojectrecycler)
    RecyclerView ongoingprojectrecycler;
    List<OnGoingProjectModel> oprojectList = new ArrayList<>();
    OnGoingProjectAdapter adapter;

    @Override
    public int setLayout() {
        return R.layout.activity_ongoing_projects;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.static_ongoing_projects), true);
        oprojectList.add(new OnGoingProjectModel("1", "मुख्य सडक निर्माण","५ करोड", "२२ भदौ २०७६", "२५ असोज २०७७", "एक वर्ष", "वडा नं. ४", "सफल थापा", "https://s26162.pcdn.co/wp-content/uploads/2016/06/road-trip.jpg", StaticValues.getDummy()));
        oprojectList.add(new OnGoingProjectModel("2", "विद्यालय निर्माण", "७ करोड", "२२ भदौ २०७६", "३१ असोज २०७७", "एक वर्ष", "वडा नं. ४", "परान मगर", "https://www.jefcoed.com/cms/lib/AL50000183/Centricity/Domain/1726/IMG_6482av-cropped.jpg", StaticValues.getDummy()));
        oprojectList.add(new OnGoingProjectModel("3", "मन्दिरको जीर्णोद्धार", "२ करोड", "२२ भदौ २०७६", "१२ असोज २०७७", "एक वर्ष", "वडा नं. ४", "हरि केसी", "https://omgnepal.com/wp-content/uploads/2019/08/fixedw_large_4x.jpg", StaticValues.getDummy()));
        oprojectList.add(new OnGoingProjectModel("4", "पुल निर्माण", "४३ लाख", "२२ भदौ २०७६", "१५ असोज २०७७", "एक वर्ष", "वडा नं. ४", "सफल केसी", "https://previews.123rf.com/images/prudek/prudek1404/prudek140400031/27129745-rope-hanging-suspension-bridge-in-nepal.jpg", StaticValues.getDummy()));
        oprojectList.add(new OnGoingProjectModel("5", "खानेपानी योजना", "३२ लाख", "२२ भदौ २०७६", "३० असोज २०७७", "एक वर्ष", "वडा नं. ४", "केसी थापा", "https://i0.wp.com/binodbhattarai.com/people/wp-content/uploads/2019/01/Melamchi.jpg?resize=900%2C597", StaticValues.getDummy()));
        oprojectList.add(new OnGoingProjectModel("6", "अस्पताल मर्मतसम्भार", "२२ करोड", "२२ भदौ २०७६", "०९ असोज २०७७", "एक वर्ष", "वडा नं. ४", "रमला थापा", "https://nepalijob.com/wp-content/uploads/2019/03/image-1.jpg", StaticValues.getDummy()));
        oprojectList.add(new OnGoingProjectModel("7", "भूमि सर्वेक्षण", "१ लाख", "२२ भदौ २०७६", "०७ असोज २०७७", "एक वर्ष", "वडा नं. ४", "माईला थापा मगर", "https://www.nepalitimes.com/wp-content/uploads/2019/05/11.jpg", StaticValues.getDummy()));
        ongoingprojectrecycler.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        adapter = new OnGoingProjectAdapter(oprojectList, getActivityContext());
        ongoingprojectrecycler.setAdapter(adapter);
    }
}
