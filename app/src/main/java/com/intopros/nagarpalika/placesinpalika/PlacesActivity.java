package com.intopros.nagarpalika.placesinpalika;

import android.app.Activity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class PlacesActivity extends AbstractActivity implements OnMapReadyCallback, PlacesAdapter.OnWodaSelectedListener {

    @BindView(R.id.rv_woda_list)
    RecyclerView rv_woda_list;

    PlacesAdapter mAdapter;
    List<PlaceModel> placeList = new ArrayList<>();

    private GoogleMap mMap;

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.static_places), true);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        List<SinglePlaceModel> singlePlaceList = new ArrayList<>();
        singlePlaceList.add(new SinglePlaceModel("मन्दिर न. १", 27.7116875, 85.3155917));
        singlePlaceList.add(new SinglePlaceModel("मन्दिर न. २", 27.6937082, 85.3198652));
        placeList.add(new PlaceModel("मन्दिर", "", singlePlaceList));

        List<SinglePlaceModel> singlePlace1List = new ArrayList<>();
        singlePlace1List.add(new SinglePlaceModel("कार्यालय न. १", 27.7112612, 85.3172654));
        singlePlace1List.add(new SinglePlaceModel("कार्यालय न. २", 27.7007701, 85.3193625));
        placeList.add(new PlaceModel("कार्यालय", "", singlePlace1List));

        List<SinglePlaceModel> singlePlace2List = new ArrayList<>();
        singlePlace2List.add(new SinglePlaceModel("शैक्षिक संस्थान न. १", 27.7111625, 85.3189413));
        singlePlace2List.add(new SinglePlaceModel("शैक्षिक संस्थान न. २", 27.7014817, 85.3238419));
        placeList.add(new PlaceModel("शैक्षिक संस्थान", "", singlePlace2List));

        List<SinglePlaceModel> singlePlace3List = new ArrayList<>();
        singlePlace3List.add(new SinglePlaceModel("फार्मेसी न. १", 27.7110547, 85.3210914));
        singlePlace3List.add(new SinglePlaceModel("फार्मेसी न. २", 27.7071225, 85.3224294));
        placeList.add(new PlaceModel("फार्मेसी", "", singlePlace3List));

        List<SinglePlaceModel> singlePlace4List = new ArrayList<>();
        singlePlace4List.add(new SinglePlaceModel("अन्य न. १", 27.7100541, 85.3221315));
        singlePlace4List.add(new SinglePlaceModel("अन्य न. २", 27.709565, 85.3223034));
        placeList.add(new PlaceModel("अन्य", "", singlePlace4List));

        rv_woda_list.setLayoutManager(new LinearLayoutManager(getActivityContext(), LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new PlacesAdapter(placeList).setListener(this);
        rv_woda_list.setAdapter(mAdapter);
    }

    @Override
    public int setLayout() {
        return R.layout.activity_places;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng woda = new LatLng(27.6462614, 85.3601788);
        mMap.addMarker(new MarkerOptions().position(woda));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(woda, 16.0f));
    }

    @Override
    public void pinLocation(List<SinglePlaceModel> placeList) {
        mMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (SinglePlaceModel item : placeList) {
            LatLng woda = new LatLng(item.getLat(), item.getLng());
            MarkerOptions marker = new MarkerOptions().position(woda).title(item.getName());
            mMap.addMarker(marker);
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int padding = 300; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);
    }
}
