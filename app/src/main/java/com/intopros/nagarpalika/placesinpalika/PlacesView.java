package com.intopros.nagarpalika.placesinpalika;

import android.view.View;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PlacesView extends RecyclerView.ViewHolder {

    public View rootView;
    @BindView(R.id.tv_ward)
    TextView tv_ward;

    public PlacesView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setItem(String title) {
        tv_ward.setText(title);
    }

    public void makeSelection(boolean selected) {
        if (selected) {
            tv_ward.setBackgroundColor(rootView.getContext().getResources().getColor(R.color.colorPrimaryDark));
            tv_ward.setTextColor(rootView.getContext().getResources().getColor(R.color.colorWhite));
        } else {
            tv_ward.setBackgroundColor(rootView.getContext().getResources().getColor(R.color.colorWhite));
            tv_ward.setTextColor(rootView.getContext().getResources().getColor(R.color.colorBlack));
        }
    }

}
