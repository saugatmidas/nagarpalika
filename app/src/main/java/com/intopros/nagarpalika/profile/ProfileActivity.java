package com.intopros.nagarpalika.profile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.CustomItem;

import butterknife.BindView;

public class ProfileActivity extends AbstractActivity {

    @BindView(R.id.iv_profile)
    ImageView iv_profile;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.ci_phone_number)
    CustomItem ci_phone_number;
    @BindView(R.id.ci_address)
    CustomItem ci_address;
    @BindView(R.id.ci_father_name)
    CustomItem ci_father_name;
    @BindView(R.id.ci_mother_name)
    CustomItem ci_mother_name;
    @BindView(R.id.ci_citizen_number)
    CustomItem ci_citizen_number;

    @Override
    public int setLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.menu_profile), true);
        Glide.with(getActivityContext())
                .load(R.drawable.ic_man)
                .apply(new RequestOptions().circleCrop())
                .into(iv_profile);

        tv_name.setText("सफल थापा");
        tv_email.setText("safal.thapa@qa.com");
        ci_phone_number.setupView("मोबाइल नम्बर", "९८०१०८३१७५", R.drawable.ic_phone);
        ci_address.setupView("ठेगाना", "बाफल, काठमाण्डु", R.drawable.ic_home_black);
        ci_father_name.setupView("बुवाको नाम", "दिपक थापा", R.drawable.ic_male_user);
        ci_mother_name.setupView("आमाको नाम", "किरण थापा", R.drawable.ic_woman);
        ci_citizen_number.setupView("नागरिकता नम्बर", "१६५४/३४४३", R.drawable.ic_card);
    }
}
