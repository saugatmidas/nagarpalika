package com.intopros.nagarpalika.topup;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class TopUpActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_topup;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Mobile Topup",true);
    }
}
