package com.intopros.nagarpalika.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intopros.nagarpalika.R;


public class CustomItem extends RelativeLayout {
    ImageView image;
    TextView label;
    TextView description;
    View rootView;

    public CustomItem(Context context) {
        super(context);
        init(context);
    }

    public CustomItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        rootView = inflate(context, R.layout.layout_custom_item, this);
        image = (ImageView) rootView.findViewById(R.id.img_ci);
        description = (TextView) rootView.findViewById(R.id.description_ci);
        label = (TextView) rootView.findViewById(R.id.label_ci);
    }

    public void setupView(int label, int description, int image) {
        this.image.setImageDrawable(getResources().getDrawable(image));
        this.label.setText(getResources().getString(label));
        this.description.setText(getResources().getString(description));
    }

    //for client profile usage
    public void setupView(String label, String description, int image) {
        this.image.setImageDrawable(getResources().getDrawable(image));
        this.label.setText(label);
        this.description.setText(description);
    }
}
