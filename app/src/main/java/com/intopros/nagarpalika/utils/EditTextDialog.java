package com.intopros.nagarpalika.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

public class EditTextDialog {

    public Dialog dialog;
    TextView tv_title;
    EditText et_review;
    TextView btn_cancel;
    public TextView btn_save;
    TextView btn_skip;

    Activity activity;
    OnDialogButtonSelect listener;

    public EditTextDialog(Activity activity, OnDialogButtonSelect buttonListener) {
        this.activity = activity;
        this.listener = buttonListener;

        dialog = new Dialog(activity, R.style.AppTheme_DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_text);
        dialog.setCancelable(false);

        tv_title = dialog.findViewById(R.id.tv_title);
        et_review = dialog.findViewById(R.id.et_review);
        btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_save = dialog.findViewById(R.id.btn_save);
        btn_skip = dialog.findViewById(R.id.btn_skip);

        btn_skip.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_save.setVisibility(View.GONE);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSuccess();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onCancel();
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null)
                    listener.onSkip();
            }
        });
    }

    public EditTextDialog setTitle(String title) {
        tv_title.setText(title);
        return this;
    }

    public String getFeedback() {
        return et_review.getText().toString();
    }

    public EditTextDialog setHint(String hint) {
        et_review.setHint(hint);
        return this;
    }

    public EditTextDialog show() {
        dialog.show();
        return this;
    }

    public EditTextDialog setOk(String positiveText) {
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText(positiveText);
        return this;
    }

    public EditTextDialog setClose() {
        btn_cancel.setText("बन्द गर्नुहोस्");
        btn_cancel.setVisibility(View.VISIBLE);
        return this;
    }
}