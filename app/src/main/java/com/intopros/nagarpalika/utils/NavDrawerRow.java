package com.intopros.nagarpalika.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

public class NavDrawerRow extends RelativeLayout {
    boolean responded = false;
    public ImageView image;
    public TextView title;
    public TextView caption;
    View rootView;

    public NavDrawerRow(Context context) {
        super(context);
        init(context);
    }

    public NavDrawerRow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        rootView = inflate(context, R.layout.row_nav_menu, this);
        image = (ImageView) rootView.findViewById(R.id.img_wpni);
        title = (TextView) rootView.findViewById(R.id.title_wpni);
        caption = (TextView) rootView.findViewById(R.id.caption_wpni);
    }

    public void setupView(int image_id, String title_text, String caption_text) {
        image.setImageResource(image_id);
        title.setText(title_text);
        caption.setText(caption_text);
    }

}