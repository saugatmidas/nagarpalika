package com.intopros.nagarpalika.utils;

public class StaticValues {

    public static String dummy ="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris auctor porttitor rutrum. Phasellus ipsum libero, dignissim ac egestas vel, sollicitudin id velit. Cras pulvinar vehicula porttitor. Fusce mollis nisl eget metus varius, sed consequat urna pharetra. Vestibulum ut diam elit. Ut dui enim, fringilla sit amet odio ut, vehicula venenatis nibh. Nulla nulla enim, finibus et nunc id, ultrices tristique nisi. Nunc ultrices turpis eu mi tincidunt ullamcorper. Sed pretium lectus sit amet nisl convallis egestas.\n" +
            "\n" +
            "Suspendisse efficitur euismod molestie. Fusce ac placerat massa. Quisque sit amet augue nisi. Mauris maximus felis metus, sit amet aliquet nunc tincidunt eu. Curabitur pretium facilisis ligula id mollis. Suspendisse non blandit est. Sed vestibulum purus vel scelerisque pretium. Maecenas tincidunt felis libero, ac finibus metus mollis sed. Curabitur arcu sem, tristique eu semper at, fermentum et est. Proin non consectetur sapien, in pharetra neque. Pellentesque ac augue faucibus, pellentesque nisi non, porta erat. Aliquam auctor nulla sodales, blandit libero scelerisque, eleifend enim. Mauris malesuada mi sed enim sagittis gravida. Morbi interdum ipsum ac eros mattis pulvinar. Aenean enim nulla, vehicula sit amet tempor non, pellentesque in quam.\n" +
            "\n" +
            "Curabitur pharetra efficitur nulla, nec pretium magna porttitor ac. Maecenas non scelerisque neque. Sed eu urna sit amet dui interdum ultricies a in ante. Duis ornare odio aliquet sollicitudin hendrerit. Cras congue, turpis in ornare scelerisque, arcu lectus laoreet urna, eget ornare arcu diam quis turpis. Donec efficitur, diam ac malesuada ornare, turpis erat bibendum purus, non lobortis massa dolor vel augue. Ut ac dapibus leo. Proin convallis, ipsum vitae venenatis rutrum, est leo ultrices diam, a hendrerit mauris magna nec metus. Aliquam tincidunt eu erat nec condimentum. Integer tortor justo, suscipit a euismod vitae, consequat ac enim. Etiam faucibus elit et ante laoreet, quis consectetur risus sollicitudin.";

    public static String[] date = {
            "2000","2001","2002","2003", "2004","2005","2006","2007", "2008","2009", "2010",
            "2011", "2012","2013", "2014","2015", "2016","2017", "2018","2019", "2020","2021",
            "2022","2023", "2024","2025", "2026","2027", "2028","2029", "2030","2031", "2032",
            "2033", "2034","2035", "2036","2037", "2038","2039", "2040","2041", "2042","2043",
            "2044","2045", "2046","2047", "2048","2049", "2050","2051", "2052","2053", "2054",
            "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064","2065",
            "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076"
    };
    public static String[] month = {
            "1","2","3","4","5","6","7","8","9","10","11","12"
    };
    public static String[] day = {
            "1","2","3","4","5","6","7","8","9","10",
            "11","12","13","14","15","16","17","18","19","20",
            "21","22","23","24","25","26","27","28","29","30"
    };
    public static String[] counters = {
            "Madhyapur Thimi","Sallaghari Bhaktapur","Chabahil Kathmandu","Syoambhu Kathmandu","Pepsicola Kathmandu"
    };
    public static String[] amounts = {
        "350","500","1000","2000","3000","4000","5000","8000","10000"
    };

    public static String[] getAmounts() {
        return amounts;
    }

    public static void setAmounts(String[] amounts) {
        StaticValues.amounts = amounts;
    }

    public static String[] getCounters() {
        return counters;
    }

    public static void setCounters(String[] counters) {
        StaticValues.counters = counters;
    }

    public static String[] getMonth() {
        return month;
    }

    public static void setMonth(String[] month) {
        StaticValues.month = month;
    }

    public static String[] getDay() {
        return day;
    }

    public static void setDay(String[] day) {
        StaticValues.day = day;
    }

    public static String[] getDate() {
        return date;
    }

    public static void setDate(String[] date) {
        StaticValues.date = date;
    }

    public static String getDummy() {
        return dummy;
    }
}
