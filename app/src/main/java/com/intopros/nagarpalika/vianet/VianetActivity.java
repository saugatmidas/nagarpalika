package com.intopros.nagarpalika.vianet;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class VianetActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_vianet;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Vianet",true);
    }
}
