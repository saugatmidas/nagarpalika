package com.intopros.nagarpalika.worldlink;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class WorldLinkActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_worldlink;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("WorldLink",true);
    }
}
